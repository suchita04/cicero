<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cicero_own');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1am2g00d');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0;;$EzyP5n(i!=[NDwQDz{: 7qqLZ5j^2[l!0vk{.Bmkf~;)E)cBR%E/:ZrinDd(');
define('SECURE_AUTH_KEY',  '+b,c`r3`8^Ko{;ME]qivtLj Ma1GaY<P!LxbxHnBS`2{FD?PvGM[>9jQDEppo~WR');
define('LOGGED_IN_KEY',    '@~1~U=R0RgJ,F(?YZ=48JqaI?Vq(#NvKyP)EW(?vIL>J3MnbB+yJiHF_XET5nTrN');
define('NONCE_KEY',        '`}g<R[MJv^OtRy7Z(~qI&#ba,9z@};Zz`~[WXDM8Yh$C d$.!5@5Mu=_M3s:epc5');
define('AUTH_SALT',        '=E^gsl1+2Ya+(-k%yCrN-iVFpF?Mn}9su5I%eH#AEeEw[rlGp2}*v}L/xymj(S+%');
define('SECURE_AUTH_SALT', ')Vk@cT^yDDgatGQjmY?2^T+$^%0BNFQj^E]iluCkUVwSPEG:G<?fDj8QwPpu,s/`');
define('LOGGED_IN_SALT',   'B/5SdLBA90,/]YvM$V5fQ!k&1@L>:0!_9[AJBk9QcfH?mx.63=Up]e(!;%/d%YYD');
define('NONCE_SALT',       'rquD?=X>}cN~N*8`I%|?Qch[<CNn0bR]$[hC?Q/FU`(dy=,]a(S:NEXxEaI6Hl]Y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
